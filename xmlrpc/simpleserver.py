#*-*coding: utf-8*-*

import RPi.GPIO as GPIO  #GPIO Bibliothek importieren
import xmlrpclib
from SimpleXMLRPCServer import SimpleXMLRPCServer

def led_on_off(on):
    print("Function called with parameter '%s'"%on
    GPIO.output(26,on == True)
    return 0

def setup():
    GPIO.setmode(GPIO.BOARD) #Pinnummerierung einstellen 
                             #  (Nummern in den Kreisen)
    GPIO.setup(26, GPIO.OUT) #Setze Pin 26 (GPIO7) als 
                             # Ausgang

    GPIO.output(26,False)     #Lege 3.3V auf Pin 26

setup()
server = SimpleXMLRPCServer(("0.0.0.0", 90))
server.register_function(led_on_off, "led_on_off")
server.serve_forever()