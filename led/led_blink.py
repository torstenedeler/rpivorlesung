import RPi.GPIO as GPIO  #GPIO Bibliothek importieren
import time                                   #Modul time importieren
GPIO.setmode(GPIO.BOARD) #Pinnummerierung einstellen 
                         #  (Nummern in den Kreisen)
GPIO.setup(26, GPIO.OUT) #Setze Pin 26 (GPIO7) als 
                         # Ausgang

lauf = 0
while True:
    lauf += 1
    
    print("[%3d] an"%lauf)
    GPIO.output(26,True)     #Lege 3.3V auf Pin 26
    time.sleep(0.5)          #Warte 500ms

    print("[%3d] aus"%lauf)
    GPIO.output(26,False)    #Lege 0V auf Pin 26
    time.sleep(0.5)
